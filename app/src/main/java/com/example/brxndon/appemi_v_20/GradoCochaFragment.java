package com.example.brxndon.appemi_v_20;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class GradoCochaFragment extends Fragment
{
    String civil = "https://cochabamba.emi.edu.bo/images/Mallas_Curriculares/INGENIERIA-CIVIL.png";
    String electronica = "https://cochabamba.emi.edu.bo/images/Mallas_Curriculares/INGENIERIA-EN-SISTEMAS-ELECTRONICOS.png";
    String agroindustrial = "https://cochabamba.emi.edu.bo/images/Mallas_Curriculares/INGENIERIA-AGROINDUSTRIAL.png";
    String comercial = "https://cochabamba.emi.edu.bo/images/Mallas_Curriculares/INGENIERIA-COMERCIAL.png";
    String petrolera = "https://cochabamba.emi.edu.bo/images/Mallas_Curriculares/INGENIERIA-PETROLERA.png";
    String sistemas = "https://cochabamba.emi.edu.bo/images/Mallas_Curriculares/INGENIERIA-DE-SISTEMAS.png";

    private Spinner lista;
    public int con = 0;

    private ArrayList<CarrerasItem> carreras;
    private CarrerasAdapter adp;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_grado_cocha, container, false);

        Bundle bundle = getArguments();
        if (bundle != null)
        {
            con = getArguments().getInt("CON", 0);
        }

        final WebView web = (WebView) view.findViewById(R.id.webgrado);
        web.setWebViewClient(new MyWebViewClient());

        WebSettings settings = web.getSettings();
        settings.setJavaScriptEnabled(true);



        //  Construccion de la lista desplegable
        initList();

        lista = (Spinner) view.findViewById(R.id.mLista);

        adp = new CarrerasAdapter(this.getActivity(), carreras);
        adp.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        lista.setAdapter(adp);

        //lista.attachDataSource(carreras);
        lista.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                CarrerasItem Select_item = (CarrerasItem) parent.getItemAtPosition(position);

                String item = Select_item.getmCarrera();

                if (con != 1)
                    Toast.makeText(getActivity(), "NO ESTA CONECTADO", Toast.LENGTH_LONG).show();

                //Toast.makeText(getActivity(), "Item"+item, Toast.LENGTH_SHORT).show();
                if (item.compareTo("Ingeniería Civil")==0 && (con == 1))
                    web.loadUrl(civil);
                if (item.compareTo("Ingeniería en Sis. Electrónicos")==0 && (con == 1))
                    web.loadUrl(electronica);
                if (item.compareTo("Ingeniería Agroindustrial")==0 && (con == 1))
                    web.loadUrl(agroindustrial);
                if (item.compareTo("Ingeniería Comercial")==0 && (con == 1))
                    web.loadUrl(comercial);
                if (item.compareTo("Ingeniería Petrolera")==0 && (con == 1))
                    web.loadUrl(petrolera);
                if (item.compareTo("Ingeniería de Sistemas")==0 && (con == 1))
                    web.loadUrl(sistemas);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

        return view;
    }

    private void initList()
    {
        carreras = new ArrayList<>();
        carreras.add(new CarrerasItem("Seleccione una carrera", R.drawable.abajo));
        carreras.add(new CarrerasItem("Ingeniería Civil", R.drawable.civil));
        carreras.add(new CarrerasItem("Ingeniería en Sis. Electrónicos", R.drawable.electronica));
        carreras.add(new CarrerasItem("Ingeniería Agroindustrial", R.drawable.industrial));
        carreras.add(new CarrerasItem("Ingeniería Comercial", R.drawable.comercial));
        carreras.add(new CarrerasItem("Ingeniería Petrolera", R.drawable.petrolera));
        carreras.add(new CarrerasItem("Ingeniería de Sistemas", R.drawable.sistemas));
    }

    private class MyWebViewClient extends WebViewClient
    {
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            view.loadUrl(url);
            return true;
        }
    }
}
