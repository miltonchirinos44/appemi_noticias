package com.example.brxndon.appemi_v_20;

public class CarrerasItem
{
    private String mCarrera;
    private int mIcon;

    public CarrerasItem(String Carrera, int Icon)
    {
        mCarrera = Carrera;
        mIcon = Icon;
    }

    public String getmCarrera()
    {
        return mCarrera;
    }

    public  int getmIcon()
    {
        return mIcon;
    }
}
