package com.example.brxndon.appemi_v_20;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;


public class NoticiasRiberaltaFragment extends Fragment
{
    private Spinner lista;
    public int con = 0;

    private ArrayList<CarrerasItem> carreras;
    private CarrerasAdapter adp;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_noticias_riberalta, container, false);

        final ImageView img = (ImageView) view.findViewById(R.id.imageUniform);
        //img.setImageResource(R.drawable.civil);

        Bundle bundle = getArguments();
        if (bundle != null)
        {
            con = getArguments().getInt("CON", 0);
        }

        //  Construccion de la lista desplegable
        initList();

        lista = (Spinner) view.findViewById(R.id.mLista);

        adp = new CarrerasAdapter(this.getActivity(), carreras);
        adp.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        lista.setAdapter(adp);

        //lista.attachDataSource(carreras);
        lista.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {

                CarrerasItem Select_item = (CarrerasItem) parent.getItemAtPosition(position);

                String item = Select_item.getmCarrera();

                if (con != 1)
                    Toast.makeText(getActivity(), "NO ESTA CONECTADO", Toast.LENGTH_LONG).show();

                //Toast.makeText(getActivity(), "Item"+item, Toast.LENGTH_SHORT).show();
                if (item.compareTo("Uniforme de Asistencia")==0 && (con == 1))
                {
                    img.setImageResource(R.drawable.uniformelp);
                }
                else if (item.compareTo("Uniforme Civil")==0 && (con == 1))
                {
                    img.setImageResource(R.drawable.civillp);
                }
                else if (item.compareTo("Uniforme Deportivo")==0 && (con == 1))
                {
                    img.setImageResource(R.drawable.deportivolp);
                }
                else if (item.compareTo("Uniforme Camuflado")==0 && (con == 1))
                {
                    img.setImageResource(R.drawable.camufladolp);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

        return view;
    }

    private void initList()
    {
        carreras = new ArrayList<>();
        carreras.add(new CarrerasItem("Seleccione Uniforme", R.drawable.abajo));
        carreras.add(new CarrerasItem("Uniforme de Asistencia", R.drawable.iconuniforme));
        carreras.add(new CarrerasItem("Uniforme Civil", R.drawable.iconcivil));
        carreras.add(new CarrerasItem("Uniforme Deportivo", R.drawable.icondeportivo));
        carreras.add(new CarrerasItem("Uniforme Camuflado", R.drawable.iconcamuflado));
    }
}
