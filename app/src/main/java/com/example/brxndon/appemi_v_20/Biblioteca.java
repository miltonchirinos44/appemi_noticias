package com.example.brxndon.appemi_v_20;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Biblioteca extends AppCompatActivity {

    String link1 = "http://www.cervantesvirtual.com/";
    String link2 = "http://www.wdl.org/es/";
    String link3 = "http://www.bdmx.mx";
    String link4 = "http://www.e-libro.com";
    String link5 = "http://www.iib.unam.mx/";

    Button bl1,bl2,bl3,bl4,bl5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biblioteca);

        //  Icono de Cabecera
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.logocabeza1);



        Bundle bundle = this.getIntent().getExtras();

        if (bundle != null)
        {
            int con = bundle.getInt("CON", 0);
            if (con != 1)
            {
                Toast.makeText(this, "NO ESTA CONECTADO", Toast.LENGTH_SHORT).show();
            }
        }

        bl1 = findViewById(R.id.link1);

        bl1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Uri uri1 = Uri.parse(link1);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri1);
                startActivity(intent);
            }
        });

        bl2 = findViewById(R.id.link2);

        bl2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Uri uri2 = Uri.parse(link2);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri2);
                startActivity(intent);
            }
        });

        bl3 = findViewById(R.id.link3);

        bl3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Uri uri3 = Uri.parse(link3);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri3);
                startActivity(intent);
            }
        });

        bl4 = findViewById(R.id.link4);

        bl4.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Uri uri4 = Uri.parse(link4);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri4);
                startActivity(intent);
            }
        });

        bl5 = findViewById(R.id.link5);

        bl5.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Uri uri5 = Uri.parse(link5);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri5);
                startActivity(intent);
            }
        });
    }
}
