package com.example.brxndon.appemi_v_20;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{
    Button btn_lp1,btn_scz1,btn_cba1,btn_rta1,btn_noticias,btn_biblioteca;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //  Icono de Cabecera
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.logocabeza1);

        //  Anular el modo horizontal
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        final Bundle bundle = new Bundle();


        if (HayConexion())
        {
            bundle.putInt("CON", 1);

            //noticiaslp.setArguments(bundlelp);
            //Toast.makeText(MainActivity.this, "ESTA CONECTADO", Toast.LENGTH_LONG).show();
        }

        //Manejador de pantallas
        btn_lp1 = findViewById(R.id.btn_lp1);

        btn_lp1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent btn_lp1 = new Intent(MainActivity.this, Principal_LaPaz.class);
                btn_lp1.putExtras(bundle);
                startActivity(btn_lp1);
            }
        });

        btn_scz1 = findViewById(R.id.btn_scz1);

        btn_scz1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent btn_scz1 = new Intent(MainActivity.this, Principal_SantaCruz.class);
                btn_scz1.putExtras(bundle);
                startActivity(btn_scz1);
            }
        });

        btn_cba1 = findViewById(R.id.btn_cbba1);

        btn_cba1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent btn_cba1 = new Intent(MainActivity.this, Principal_Cocha.class);
                btn_cba1.putExtras(bundle);
                startActivity(btn_cba1);
            }
        });

        btn_rta1 = findViewById(R.id.btn_rta1);

        btn_rta1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent btn_rta1 = new Intent(MainActivity.this, Principal_Ribe.class);
                btn_rta1.putExtras(bundle);
                startActivity(btn_rta1);
            }
        });

        btn_noticias = findViewById(R.id.btn_not);

        btn_noticias.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent btn_noticias = new Intent(MainActivity.this, Principal_Noticias.class);
                btn_noticias.putExtras(bundle);
                startActivity(btn_noticias);
            }
        });

        btn_biblioteca = findViewById(R.id.btn_biblioteca);

        btn_biblioteca.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent btn_biblioteca = new Intent(MainActivity.this, Biblioteca.class);
                btn_biblioteca.putExtras(bundle);
                startActivity(btn_biblioteca);
            }
        });
    }

    public boolean HayConexion()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = cm.getActiveNetworkInfo();

        if( networkInfo != null && networkInfo.isConnected())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
