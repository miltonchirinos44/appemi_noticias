package com.example.brxndon.appemi_v_20;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CarrerasAdapter extends ArrayAdapter
{
    public CarrerasAdapter(Context context, ArrayList<CarrerasItem> CarreasList)
    {
        super(context, 0, CarreasList);
    }

    @NonNull
    @Override
    public View getView(int position, @NonNull View convertView, @NonNull ViewGroup parent)
    {
        return initView(position, convertView, parent);
    }
    @Override
    public View getDropDownView(int position, @NonNull View convertView, @NonNull ViewGroup parent)
    {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.carreras_lista, parent, false
            );
        }

        ImageView imagen = convertView.findViewById(R.id.icon);
        TextView carrera = convertView.findViewById(R.id.titulo);

        CarrerasItem items = (CarrerasItem) getItem(position);

        if (items != null)
        {
            imagen.setImageResource(items.getmIcon());
            carrera.setText(items.getmCarrera());
        }

        return convertView;
    }
}
