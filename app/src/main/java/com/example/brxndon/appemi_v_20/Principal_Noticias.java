package com.example.brxndon.appemi_v_20;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class Principal_Noticias extends AppCompatActivity
{

    String url = "http://sello.emi.edu.bo/android/public/detalle2";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal__noticias);

        //  Icono de Cabecera
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.logocabeza1);

        //  Anular el modo horizontal
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Bundle bundle = this.getIntent().getExtras();

        if (bundle != null)
        {
            int con = bundle.getInt("CON", 0);
            if (con == 1)
            {
                WebView web = (WebView) findViewById(R.id.web1);
                web.setWebViewClient(new MyWebViewClient());

                WebSettings settings = web.getSettings();
                settings.setJavaScriptEnabled(true);
                settings.setDomStorageEnabled(true);

                web.loadUrl(url);
            }
            else
            {
                Toast.makeText(this, "NO ESTA CONECTADO", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class MyWebViewClient extends WebViewClient
    {
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            view.loadUrl(url);
            return true;
        }
    }
}
