package com.example.brxndon.appemi_v_20;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import javax.security.auth.callback.PasswordCallback;


public class PosgradoLaPazFragment extends Fragment
{
    private Spinner lista;

    private ArrayList<CarrerasItem> carreras;
    private CarrerasAdapter adp;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_posgrado_la_paz, container, false);

        Bundle bundle = getArguments();

        if (bundle != null)
        {
            int con = getArguments().getInt("CON", 0);
            if (con != 1)
            {
                Toast.makeText(getActivity(), "NO ESTA CONECTADO", Toast.LENGTH_SHORT).show();
            }
        }

        final TextView t = (TextView) view.findViewById(R.id.modalidad);
        final TextView t0 = (TextView) view.findViewById(R.id.textnombre);
        final TextView t1 = (TextView) view.findViewById(R.id.textmod);
        final TextView t2 = (TextView) view.findViewById(R.id.textduracion);
        final TextView t3 = (TextView) view.findViewById(R.id.texthora);
        final Button b = (Button) view.findViewById(R.id.numero);
        final Button b1 = (Button) view.findViewById(R.id.numero2);
        final Button b2 = (Button) view.findViewById(R.id.numero3);

        b.setText("2-435285");
        b1.setText("2-113054");
        b2.setText("71294598");

        //  Construccion de la lista desplegable
        initList();

        lista = (Spinner) view.findViewById(R.id.mLista);

        adp = new CarrerasAdapter(this.getActivity(), carreras);
        adp.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        lista.setAdapter(adp);

        //lista.attachDataSource(carreras);
        lista.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                CarrerasItem Select_item = (CarrerasItem) parent.getItemAtPosition(position);

                String item = Select_item.getmCarrera();

                if (item.compareTo("Maestria en Gestion Ambiental")==0)
                {
                    t.setText("MAESTRIA");
                    t0.setText("Gestion Ambiental y Recursos Naturales");
                    t1.setText("PRESENCIAL");
                    t2.setText("");
                    t3.setText("");

                }
                if (item.compareTo("Maestria en Administracion")==0)
                {
                    t.setText("MAESTRIA");
                    t0.setText("Administracion de Empresas");
                    t1.setText("PRESENCIAL");
                    t2.setText("");
                    t3.setText("");
                }
                if (item.compareTo("Maestria en Seguridad en TIC")==0)
                {
                    t.setText("MAESTRIA");
                    t0.setText("Seguridad en Tecnologias de la Informacion");
                    t1.setText("PRESENCIAL");
                    t2.setText("");
                    t3.setText("");
                }
                if (item.compareTo("Maestria en Derecho Constitucional")==0)
                {
                    t.setText("MAESTRIA");
                    t0.setText("Derecho Constitucional y Gestion Publica");
                    t1.setText("PRESENCIAL");
                    t2.setText("");
                    t3.setText("");
                }
                if (item.compareTo("Maestria en Ingenieria Estructural")==0)
                {
                    t.setText("MAESTRIA");
                    t0.setText("Ingenieria Estructural");
                    t1.setText("PRESENCIAL");
                    t2.setText("");
                    t3.setText("");
                }

                //  DIPLOMADOS
                if (item.compareTo("Diplomado en Ingenieria Legal")==0)
                {
                    t.setText("DIPLOMADO");
                    t0.setText("Ingenieria Legal");
                    t1.setText("PRESENCIAL");
                    t2.setText("5 MESES");
                    t3.setText("Lun - Vie / 19:00 - 22:00");
                }
                if (item.compareTo("Diplomado en Educacion Superior")==0)
                {
                    t.setText("DIPLOMADO");
                    t0.setText("Educacion Superior");
                    t1.setText("VIRTUAL");
                    t2.setText("5 MESES");
                    t3.setText("DISPONIBLE 24 Hrs.");
                }
                if (item.compareTo("Diplomado en Planific. y Desarrollo")==0)
                {
                    t.setText("DIPLOMADO");
                    t0.setText("Planificacion y Desarrollo de Competencias Profesionales en Educacion Superior");
                    t1.setText("VIRTUAL / PRESENCIAL");
                    t2.setText("5 MESES");
                    t3.setText("24 Hrs. / Lun Vie - 19:00  22:00");
                }
                if (item.compareTo("Diplomado en Transparencia")==0)
                {
                    t.setText("DIPLOMADO");
                    t0.setText("Transparencia y Lucha contra la Corrupcion");
                    t1.setText("PRESENCIAL");
                    t2.setText("5 MESES");
                    t3.setText("Sab / 7:30 - 15:30");
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(Intent.ACTION_CALL, Uri.parse("tel:22435285"));
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) !=
                        PackageManager.PERMISSION_GRANTED)
                    return;
                startActivity(i);
            }
        });

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(Intent.ACTION_CALL, Uri.parse("tel:22113054"));
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) !=
                        PackageManager.PERMISSION_GRANTED)
                    return;
                startActivity(i);
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(Intent.ACTION_CALL, Uri.parse("tel:71294598"));
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) !=
                        PackageManager.PERMISSION_GRANTED)
                    return;
                startActivity(i);
            }
        });

        return view;
    }

    private void initList()
    {
        carreras = new ArrayList<>();
        carreras.add(new CarrerasItem("Seleccione un Curso", R.drawable.abajo));
        carreras.add(new CarrerasItem("Maestria en Gestion Ambiental", R.drawable.curso));
        carreras.add(new CarrerasItem("Maestria en Administracion", R.drawable.curso));
        carreras.add(new CarrerasItem("Maestria en Seguridad en TIC", R.drawable.curso));
        carreras.add(new CarrerasItem("Maestria en Derecho Constitucional", R.drawable.curso));
        carreras.add(new CarrerasItem("Maestria en Ingenieria Estructural", R.drawable.curso));
        carreras.add(new CarrerasItem("Diplomado en Ingenieria Legal", R.drawable.curso));
        carreras.add(new CarrerasItem("Diplomado en Educacion Superior", R.drawable.curso));
        carreras.add(new CarrerasItem("Diplomado en Planific. y Desarrollo", R.drawable.curso));
        carreras.add(new CarrerasItem("Diplomado en Transparencia", R.drawable.curso));
    }
}
